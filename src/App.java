import model.Author;
import model.Book;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Author author1 = new Author("Nguyen Van Nam", "namnv@gmail.com", 'm');
        System.out.println(author1);

        Author author2 = new Author("Tran Thu Trang", "trangtt@gmail.com", 'f');
        //author2.setEmail("trangtt@gmail.com");
        System.out.println(author2);

        Book book1 = new Book("Java", author1, 150000);
        System.out.println(book1);

        Book book2 = new Book("NodeJS", author2, 240000, 10);
        System.out.println(book2);
    }
}
